#!/bin/bash
#check if we have a working virtualenv
#recreate if necessary
if [ ! -f resource/env/bin/activate ]; then
    rm -rf resource
    mkdir resource
    virtualenv -p $(which python3) resource/env
fi

#update the venv using the requirements file
source resource/env/bin/activate
pip install --upgrade pip
pip install -r build/requirements.pip
