#SKA deployment

The deployment script is designed to run in the environment set up by the
scripts in the `first-time` folder.

This is connection between ports, sockets and python apps:

    localhost:5000      :8000/tm        :8000/testtm
             +             +             +
             |             |             |
             |             |             |
          +---------------------------------+
          |  |             |             |  |
          |  |           NGINX           |  |
          |  |             |             |  |
          +---------------------------------+
             |             |             |
             |             |             |
             +             +             +
       aavs.sock        tm.sock        testtm.sock
             +             +             +
             |             |             |
             |             |             |
          +---------------------------------+
          |  |             |             |  |
          |  |          UWSGI            |  |
          |  |             |             |  |
          +---------------------------------+
             |             |             |
             +             +             +
          flask         django         django
           app           app            app

The relevant files live (by default) under $HOME/skadeploy. The folder structure is created by the first-time scripts

To deploy to tm, run:

     ./build/deploy -m /tm

You can only deploy to /tm or /testtm because, even though the sockets are created dynamically by UWSGI, NGINX is configured with a hardcoded connection between urls and sockets.
