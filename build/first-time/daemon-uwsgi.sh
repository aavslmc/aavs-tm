#!/bin/bash
#configure uwsgi to load in emperor mode when ubuntu starts up
#see http://uwsgi-docs.readthedocs.org/en/latest/Emperor.html

#default deployment locations
bindir=$HOME/bin
vassaldir=$HOME/skadeploy/uwsgi/vassals
logdir=$HOME/log/uwsgi
user=$(whoami)
group=$(id -g -n)

function show_help {
echo "Usage: daemon-uwsgi [-b BINDIR] [-v VASSALDIR] [-u USER] [-g GROUP] [-l
LOGDIR]"
echo "Defaults are $bindir and $vassaldir and current user and group."
}

#update default locations from command line arguments
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

while getopts "h?b:v:u:g:l:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    b)  bindir=$OPTARG
        ;;
    v)  vassaldir=$OPTARG
        ;;
    u)  user=$OPTARG
        ;;
    g)  group=$OPTARG
        ;;
    l)  logdir=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))
[ "$1" = "--" ] && shift

execpath=$bindir/uwsgi

#create folder for logging
mkdir -p $logdir

#create file in /etc/init from template
sed \
    -e "s:%%exec%%:$execpath:" \
    -e "s:%%vassals%%:$vassaldir:" \
    -e "s:%%logdir%%:$logdir:" \
    -e "s:%%user%%:$user:" \
    -e "s:%%group%%:$group:" \
    -e "s:%%vassals%%:$vassaldir:" \
uwsgi.template.conf > /etc/init/uwsgi.conf

#change permissions so you don't need sudo to view log files
chown -R $user:$group $logdir
chmod -R a+r $logdir



