#!/bin/bash
#Create config file for tm sites
#This file is read by nginx
#It handles both /tm and /testtm

#default file location
nginxdir=$HOME/skadeploy/nginx
ip=$(ifconfig | grep -oP "(?<=inet addr:).*?(?=Bcast)")

function show_help {
echo "Usage: setup-nginx-tm-emulator [-n NGINXDIR]"
echo "Defaults is $nginxdir"
}

#update default file location from command line if necesary
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

while getopts "h?n:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    n)  nginxdir=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))
[ "$1" = "--" ] && shift

echo "Setting up tm-emulator in $nginxdir".

#create all the required folders
#sites: for the nginx config files
#sockets: for the sockets to communicate between nginx and uwsgi
#static: for hosting static files to be served with out calling uwsgi
#params: for uwsgi params
mkdir -p $nginxdir/{sites,sockets,static,params}
mkdir -p $nginxdir/static/tm-emulator/{static,media}

#create nginx config file from a template and place in correct location
sed \
    -e "s:%%upstream%%:tmemulator:" \
    -e "s:%%socketdir%%:$nginxdir/sockets:" \
    -e "s/%%port%%/8000/" \
    -e "s/%%server%%/$ip/" \
    -e "s:%%mount%%:/tm:" \
    -e "s:%%static%%:$nginxdir/static/tm-emulator:" \
    -e "s:%%media%%:$nginxdir/media/tm-emulator:" \
    -e "s:%%params%%:$nginxdir/params/uwsgi_params:" \
    nginx.template.conf > $nginxdir/sites/tm-emulator_nginx.conf

cp uwsgi_params $nginxdir/params
