#!/bin/bash
#Create symlinks for nginx sites-enabled
#nginx will serve the sites which have config files in /etc/nginx/sites-enabled
#This script should be run after the config files have been created under $HOME/skadeploy.
#It will create a symlink for all site under $nginxdir/sites
#It might be necessary to run `sudo service nginx restart` afterwards.

#default location
nginxdir=$HOME/skadeploy/nginx

function show_help {
echo "Usage: enable-sites [-n NGINXDIR]"
echo "Defaults is $nginxdir"
}

#update default location from command line if necessary
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

while getopts "h?n:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    n)  nginxdir=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))
[ "$1" = "--" ] && shift

echo "Enabling sites from $nginxdir/sites/"

#create symlinks
for site in $nginxdir/sites/*
do
    ln -s $site /etc/nginx/sites-enabled
done
