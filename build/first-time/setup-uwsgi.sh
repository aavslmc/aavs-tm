#!/bin/bash
#install and configure uwsgi program
#(we don't use the system uwsgi because we want to manage the plugins directly)

#default locations
bindir=$HOME/bin
plugindir=$HOME/skadeploy/uwsgi-plugins
vassaldir=$HOME/skadeploy/uwsgi/vassals
nginxdir=$HOME/skadeploy/nginx

mydir="$( cd  "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function show_help {
echo "Usage: install-uwsgi [-b BINDIR] [-p PLUGINDIR] [-v VASSALDIR]"
echo "Defaults are $bindir and $plugindir $vassaldir"
}


#update default locations from command line if necessary
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

while getopts "h?bpv:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    b)  bindir=$OPTARG
        ;;
    p)  plugindir=$OPTARG
        ;;
    v)  vassaldir=$OPTARG
        ;;
    esac
done

shift $((OPTIND-1))
[ "$1" = "--" ] && shift

echo "Installing uwsgi to $bindir and plugins to $plugindir".
echo "$vassaldir will be created for vassals"


#Set up temporary work directory
tempdir=$(mktemp -d)
function cleanup {
rm -rf "$tempdir"
echo "Deleted temp working directory $tempdir"
}
trap cleanup EXIT

cd $tempdir
if [ $? -ne 0 ]; then { echo "Failed, aborting." ; exit 1; } fi
pwd

#Download uwsgi source
wget http://projects.unbit.it/downloads/uwsgi-latest.tar.gz
if [ $? -ne 0 ]; then { echo "Download of uwsgi failed, aborting." ; exit 1; } fi

tar zxf uwsgi-latest.tar.gz
cd $(find . -type d -iname "uwsgi-*")

#Build uwsgi and python libraries
make PROFILE=nolang
PYTHON=python3.4 ./uwsgi --build-plugin "plugins/python python34"
PYTHON=python2.7 ./uwsgi --build-plugin "plugins/python python27"

#install to required locations
mkdir -p $bindir
echo "Copying $uwsgi to $bindir ..."
cp uwsgi $bindir/

mkdir -p $plugindir
echo "Copying plugins to $plugindir ..."
cp python*_plugin.so $plugindir

mkdir -p $vassaldir

mkdir -p $nginxdir/params
cp $mydir/uwsgi_params $nginxdir/params
