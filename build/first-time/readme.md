#One time setup for SKA project

The host environment must provide (via apt-get):
python2.7
python3.4
python-devel (development headers)
nginx
ifconfig

Then, to set up directory structures, run:

    ./setup-uwsgi
    ./setup-nginx-mock-api
    ./setup-nginx-tm-emulator

To set up server, run :
    
    sudo ./enable-sites
    sudo ./daemon-uwsgi

If you are running everything as the same user, then the scripts can be run as
is.
If you want to use a user without sudo privileges for deployment, then run thej
first three scripts as the basic user and switch to the administrator account
for the sudo scripts.
In this case, you will need to override the default folder locations for the sudo scripts.
Run them with the -h flag to see the options.
