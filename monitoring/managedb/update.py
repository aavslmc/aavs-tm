from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from ..models import Rack, Component, Property, Event, Command, Parameter, EmailSettings

import dateutil.parser as parser
import json
import requests


class ComponentStateError(Exception):
    """Exception for an unexpected component state"""
    pass


def get_email_settings():
    """
    Check database and either create (with defaults) or get emails settings
    :return: email settings model
    """
    # get the current email settings from database
    if not EmailSettings.objects.all():
        # no settings, create default with settings from django settings file
        email = EmailSettings.objects.create()
    else:
        # get values from the setting object
        email = EmailSettings.objects.all()[0]

    return email


def update_email_settings(email, host, port, user, password, use_tls):
    """
    Change the values stored in the email settings database table
    :param email: Email settings object from database
    :param host: new value of host
    :param port: new value of port
    :param user: new value for username
    :param password: new value for password
    :return: None
    """

    email.host = host
    email.port = port
    email.user = user
    email.password = password
    email.use_tls = use_tls
    email.save()

    return None


def get_response_from_json(url):
    """
    Get text JSON response from REST API

    :param url:  A url string to query
    :return: String containing JSON response from the REST API
    """

    req = requests.get(url)
    return json.loads(req.text)


def get_component_status(url):
    """
    Query API for status of component at desired URL
    :param url: the URL to poll the API at
    :return: String status, "ok", "alarm" or "fault", enforced lowercase
    """

    req = requests.get(url)
    response = json.loads(req.text)

    return parse_component_status(response['status']['value'].lower())


def parse_component_status(status_string):
    """
    Convert the string from API reponse into the component status choices we have declared
    :param status_string: string containing the API according to response
    :return: state; Component.STATUS_CHOICES member
    """

    if status_string == Component.ALARM:
        state = Component.ALARM
    elif status_string == Component.FAULT:
        state = Component.FAULT
    elif status_string == Component.OK:
        state = Component.OK
    else:
        raise ComponentStateError('Unexpected component state: {0}'.format(status_string))

    return state


def update_racks():
    """
    Update racks in the database
    :return: Return textdata string
    """

    t = get_response_from_json(settings.LMC_API_URL['rack'])

    # delete racks which aren't in the API anymore
    for rack in Rack.objects.all():
        match = next((r for r in t['racks'] if r['component_id'] == rack.component_id), None)
        if match is None:
            # no matches - delete
            rack.delete()

    # create / update racks
    for rack in t['racks']:
        # fetch state information
        url = '/'.join([settings.LMC_API_URL['rack'],
                        str(rack['component_id']),
                        'status'])

        state = get_component_status(url)

        # no problems: update or create the rack
        Rack.objects.update_or_create(component_id=rack['component_id'],
                                      component_type="rack",
                                      defaults={'status': state})

    return t


def update_properties(component_object, properties):
    """
    Add and remove properties in database
    :param component_object: component we're looking at
    :param properties: all properties in API result
    :return: None
    """
    clean_up_properties(properties, component_object)
    for prop in properties:
        add_new_property(component_object, prop)

    return None


def add_new_property(component_object, prop):
    """
    Create property object and add to database if it's new
    :param component_object:
    :param co_type:
    :param prop:
    :return: None
    """

    n = prop['name']
    try:
        t = prop['type']
    except KeyError:
        t = 'Property'
    try:
        v = prop['value']
    except KeyError:
        v = 'N/A'

    co_type = ContentType.objects.get_for_model(component_object)

    # if component_object.component_id doesn't match the property, return
    i = component_object.component_id

    try:
        property_component_id = prop['component_id']
    except KeyError:
        # property is badly formed. Do nothing with it
        return None

    if i == property_component_id:

        matches = Property.objects.filter(content_type__pk=co_type.id,
                                          object_id=component_object.id,
                                          property_name=n,
                                          property_type=t)
        # no match - create
        if not matches:
            p = Property(content_object=component_object,
                         property_name=n,
                         property_value=v,
                         property_type=t)
            p.save()
        # matches - update
        else:
            # loop over matches (should only be one!) and update value
            for p in matches:
                p.property_value = v
                p.save()

    return None


def clean_up_properties(properties, component_object):
    """
    Remove any properties which are in the database but no longer defined by the API
    :param properties: list of property dictionaries
    :param component_object: component we're looking at
    :return: None
    """

    co_type = ContentType.objects.get_for_model(component_object)
    for prop in Property.objects.filter(content_type__pk=co_type.id, object_id=component_object.id):
        match = next((p for p in properties if p['name'] == prop.property_name), None)
        if match is None:
            # no matches - delete
            prop.delete()
    return None


def update_events(component, events):
    """
    Remove and add evnts to component
    :param component: component we're querying
    :param events: list of events returned by API
    :return: None
    """
    clean_up_events(component, events)
    for event in events:
        add_new_event(component, event)

    return None


def add_new_event(component, event):
    """
    Add or modify event for component
    :param component: The component linked to the event
    :param event: The event dictionary
    :return: None
    """

    updated_values = {'name': event['name'],
                      'type': event['type'],
                      'severity': event['severity'],
                      'timestamp': parser.parse(event['timestamp'])}

    Event.objects.update_or_create(component=component,
                                   event_id=event['event_id'],
                                   defaults=updated_values)

    return None


def clean_up_events(component, events):
    """
    Remove any events not in the API response
    :param component: compoennt we're querying
    :param events: events to check for
    :return: None
    """

    for event in component.event_set.all():
        match = next((e for e in events if event.event_id == e['event_id']), None)
        if match is None:
            # no matches - delete
            event.delete()
    return None


def update_commands(component, commands):
    """
    Add and remove commands as necessary from database
    :param component: component we're querying
    :param commands: list of commands from API response
    :return: None
    """
    clean_up_commands(component, commands)
    for command in commands:
        add_new_command(component, command)
    return None


def add_new_command(component, command):
    """
    Add or modify command for component
    :param component: The component linked to the command
    :param command: The command dictionary
    :return: None
    """

    # update or create the command
    command_object, created = Command.objects.update_or_create(component=component,
                                                               name=command['name'])
    command_type = ContentType.objects.get_for_model(command_object)

    # get/create parameter objects
    for parameter in command['parameters']:
        p = Parameter(content_object=command_object,
                      name=parameter['name'],
                      type=parameter['type'])

        if not Parameter.objects.filter(content_type__pk=command_type.id,
                                        object_id=command_object.id,
                                        name=parameter['name'],
                                        type=parameter['type']):
            p.save()

    return None


def clean_up_commands(component, commands):
    """
    Remove commands from component
    :param component: component we're querying
    :param commands: commands from API response
    :return: None
    """
    for command in component.command_set.all():
        match = next((c for c in commands if command.name == c['name']), None)
        if match is None:
            # no matches - delete
            command.delete()

    return None


def update_rack_number(rack_number):
    """
    Update components and properties for a specific rack
    :param rack_number: id number for the rack
    :return: String textdata, rack object
    """

    textdata = get_response_from_json('/'.join([settings.LMC_API_URL['rack'],
                                                rack_number]))
    textdata = textdata['rack']
    rack, created = Rack.objects.get_or_create(component_id=rack_number, component_type="rack")

    # remove any components which aren't in API anymore
    for component in rack.component_set.all():
        match = next((c for c in textdata['members'] if c['component_id'] == component.component_id), None)
        if match is None:
            # no matches - delete
            component.delete()

    # gather components in the rack
    for member in textdata['members']:

        url = '/'.join([settings.LMC_API_URL[member['component_type']],
                        str(member['component_id']),
                        'status'])
        state = get_component_status(url)

        updated_values = {'row': member['row'],
                          'col': member['column'],
                          'width': member['width'],
                          'height': member['height'],
                          'status': state}

        comp, created = Component.objects.update_or_create(component_type=member['component_type'],
                                                           component_id=member['component_id'],
                                                           defaults=updated_values)
        rack.component_set.add(comp)

    return textdata, rack
