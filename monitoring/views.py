from django.conf import settings
from django.core.mail.backends.smtp import EmailBackend
from django.core.mail import send_mail, BadHeaderError
from django.shortcuts import render

import smtplib

from .models import Rack, Component
from .forms import EmailForm, EmailSettingsForm
from .managedb import update


def send_email_test(request):

    # Borrowed liberally from
    # http://stackoverflow.com/questions/28400943/python-django-e-mail-form-example

    email = update.get_email_settings()

    # define the backend
    backend = EmailBackend(host=email.host,
                           port=email.port,
                           username=email.user,
                           password=email.password,
                           use_tls=email.use_tls)

    # send the email using the settings
    if request.method == 'GET':
        form = EmailForm(email=email)
    else:
        form = EmailForm(request.POST, email=email)
        if form.is_valid():
            sender = form.cleaned_data['sender']
            subject = form.cleaned_data['subject']
            recipient = form.cleaned_data['recipient']
            message = form.cleaned_data['message']
            try:
                send_mail(subject,
                          message,
                          sender,
                          [recipient],
                          fail_silently=False,
                          connection=backend)
            except BadHeaderError:
                return render(request, 'monitoring/email_test.html', {'form': form,
                                                                      'sent': False,
                                                                      'message': 'Bad Header Error'})
            except smtplib.SMTPException as e:
                return render(request, 'monitoring/email_test.html', {'form': form,
                                                                      'sent': False,
                                                                      'message': str(e)})
            form = EmailForm(email=email)
            return render(request, 'monitoring/email_test.html', {'form': form,
                                                                  'sent': True})

    return render(request, 'monitoring/email_test.html', {'form': form})


def email_settings(request):

    email = update.get_email_settings()

    if request.method == 'GET':
        form = EmailSettingsForm(email=email)
    else:
        form = EmailSettingsForm(request.POST, email=email)
        if form.is_valid():
            # save the settings values
            host = form.cleaned_data['host']
            port = form.cleaned_data['port']
            user = form.cleaned_data['user']
            password = form.cleaned_data['password']
            use_tls = form.cleaned_data['use_tls']

            update.update_email_settings(email, host, port, user, password, use_tls)

    return render(request, 'monitoring/email_settings.html', {'form': form})


def datacentre_view(request):

    update.update_racks()

    # get all racks and load view
    racks = Rack.objects.all()
    return render(request, 'monitoring/datacentre.html', {'racks': racks})


def rack_view(request, rack_number, component_type=None, component_id=None):

    # if component_type="All", then actually want "none"
    if component_type == "All":
        component_type = None

    property_data, rack = update.update_rack_number(rack_number)
    # rack view has no events or commands by default
    event_data = {'events': []}
    command_data = {'commands': []}

    # properties for the current view, default to rack
    co = [rack]
    if component_type is not None and component_id is not None:

        # we're looking at a component. Query it!
        url = '/'.join([settings.LMC_API_URL[component_type], component_id])
        property_data = update.get_response_from_json(url)
        event_data = update.get_response_from_json('/'.join([url, 'events']))
        command_data = update.get_response_from_json('/'.join([url, 'commands']))

        if component_type != 'tile':
            property_data = property_data['component']
        else:
            property_data = property_data['tile']

        # set co to this component
        co = Component.objects.filter(rack=rack, component_type=component_type, component_id=component_id)
        component_id = int(component_id)

    elif component_type is not None:
        # querying for all components of type "component_type"
        co = Component.objects.filter(rack=rack, component_type=component_type)
        property_data = update.get_response_from_json('/'.join([settings.LMC_API_URL[component_type],
                                                                'properties']))
        event_data = update.get_response_from_json('/'.join([settings.LMC_API_URL[component_type],
                                                             'events']))
        command_data = update.get_response_from_json('/'.join([settings.LMC_API_URL[component_type],
                                                               'commands']))

    for component_object in co:

        update.update_properties(component_object, property_data['properties'])
        # racks don't have commands or events
        if component_object != rack:
            update.update_events(component_object, event_data['events'])
            update.update_commands(component_object, command_data['commands'])

    # load view with updated rack
    components = rack.component_set.all()
    component_types = list(set(comp.component_type for comp in components))
    component_types.insert(0, "All")

    # collect all properties and flatten into 1-d list
    properties2d = [c.properties.all() for c in co]
    properties = [x for sublist in properties2d for x in sublist]

    # collect events and commands, and flatten them
    if component_type is not None:
        events2d = [c.event_set.all() for c in co]
        events = [x for sublist in events2d for x in sublist]

        commands2d = [c.command_set.all() for c in co]
        commands = [x for sublist in commands2d for x in sublist]
    else:
        # at rack level, there are no events or commands
        events = None
        commands = None

    return render(request, 'monitoring/rack.html',
                           {'components': components,
                            'rack': rack,
                            'component_types': component_types,
                            'properties': properties,
                            'events': events,
                            'commands': commands,
                            'selected_comp_id': component_id,
                            'selected_comp_type': component_type})
